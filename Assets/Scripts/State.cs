using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; 

public class State
{   
    
    // 'States' that the enemy could be in.
    public enum STATE
    {
        IDLE, PATROL, PURSUE, ATTACK, SLEEP, TAKINGDAMAGE
    };

    // 'Events' - where we are in the running of a STATE.
    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name; 
    protected EVENT stage; 
    protected GameObject enemy;
    // Location of the player. 
    // This will allow the enemy tank to know where the player is, 
    // so it can Rotate to the player and know whether it should be shooting or chasing; depending on the distance.
    protected Transform player; 
    protected State nextState; 
    protected NavMeshAgent agent; // To store the enemy NavMeshAgent component.

    float visDist = 10.0f; // When the player is within a distance of 10 from the enemy, then the enemy should be able to see it...
    float visAngle = 30.0f; // ...if the player is within 30 degrees of the line of sight.
    float shootDist = 7.0f; // When the player is within a distance of 7 from the enemy, then the enemy can go into an ATTACK state.

    // Constructor for State
    public State(GameObject _enemy, NavMeshAgent _agent, Transform _player)
    {
        enemy = _enemy;
        agent = _agent;
        stage = EVENT.ENTER;
        player = _player;
    }

    // Phases as you go through the state.
    public virtual void Enter() { stage = EVENT.UPDATE; } // Runs first whenever you come into a state and sets the stage to whatever is next, so it will know later on in the process where it's going.
    public virtual void Update() { stage = EVENT.UPDATE; } // Once you are in UPDATE, you want to stay in UPDATE until it throws you out.
    public virtual void Exit() { stage = EVENT.EXIT; } // Uses EXIT so it knows what to run and clean up after itself.

    // The method that will get run from outside and progress the state through each of the different stages.
    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState; // Notice that this method returns a 'state'.
        }
        return this; // If we're not returning the nextState, then return the same state.
    }

    // Can the enemy see the player, using a simple Line Of Sight calculation?
    public bool CanSeePlayer()
    {
        Vector3 direction = player.position - enemy.transform.position; // Provides the vector from the enemy to the player.
        float angle = Vector3.Angle(direction, enemy.transform.forward); // Provide angle of sight.

        // If player is close enough to the enemy AND within the visible viewing angle...
        if(direction.magnitude < visDist && angle < visAngle)
        {
            return true; // enemy CAN see the player.
        }
        return false; // enemy CANNOT see the player.
    }

    public bool CanAttackPlayer()
    {
        Vector3 direction = player.position - enemy.transform.position; // Provides the vector from the enemy to the player.
        if(direction.magnitude < shootDist)
        {
            return true; // enemy IS close enough to the player to attack.
        }
        return false; // enemy IS NOT close enough to the player to attack.
    }
}

// Constructor for Idle state.
public class Idle : State
{
    public Idle(GameObject _enemy, NavMeshAgent _agent, Transform _player)
                : base(_enemy, _agent, _player)
    {
        name = STATE.IDLE; // Set name of current state.
    }

    public override void Enter()
    {
        // anim.SetTrigger("isIdle"); // Sets any current animation state back to Idle.
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {
        if (CanSeePlayer())
        {
            nextState = new Pursue(enemy, agent, player);
            stage = EVENT.EXIT; // The next time 'Process' runs, the EXIT stage will run instead, which will then return the nextState.
        }
        // The only place where Update can break out of itself. Set chance of breaking out at 10%.
        else if(Random.Range(0,100) < 10)
        {
            nextState = new Patrol(enemy, agent, player);
            stage = EVENT.EXIT; // The next time 'Process' runs, the EXIT stage will run instead, which will then return the nextState.
        }
    }

    public override void Exit()
    {
        //anim.ResetTrigger("isIdle"); // Makes sure that any events queued up for Idle are cleared out.
        base.Exit();
    }
}

// Constructor for Patrol state.
public class Patrol : State
{
    int currentIndex = -1;
    public Patrol(GameObject _enemy, NavMeshAgent _agent, Transform _player)
                : base(_enemy, _agent, _player)
    {
        name = STATE.PATROL; // Set name of current state.
        agent.speed = 2; // How fast your character moves ONLY if it has a path. Not used in Idle state since agent is stationary.
        agent.isStopped = false; // Start and stop agent on current path using this bool.
    }

    public override void Enter()
    {
        float lastDist = Mathf.Infinity; // Store distance between enemy and waypoints.

        // Calculate closest waypoint by looping around each one and calculating the distance between the enemy and each waypoint.
        for (int i = 0; i < GameEnvironment.Singleton.Checkpoints.Count; i++)
        {
            GameObject thisWP = GameEnvironment.Singleton.Checkpoints[i];
            float distance = Vector3.Distance(enemy.transform.position, thisWP.transform.position);
            if(distance < lastDist)
            {
                currentIndex = i - 1; // Need to subtract 1 because in Update, we add 1 to i before setting the destination.
                lastDist = distance;
            }
        }
        // anim.SetTrigger("isWalking"); // Start agent walking animation.
        base.Enter();
    }

    public override void Update()
    {
        // Check if agent hasn't finished walking between waypoints.
        if(agent.remainingDistance < 1)
        {
            // If agent has reached end of waypoint list, go back to the first one, otherwise move to the next one.
            if (currentIndex >= GameEnvironment.Singleton.Checkpoints.Count - 1)
                currentIndex = 0;
            else
                currentIndex++;

            agent.SetDestination(GameEnvironment.Singleton.Checkpoints[currentIndex].transform.position); // Set agents destination to position of next waypoint.
        }

        if (CanSeePlayer())
        {
            nextState = new Pursue(enemy, agent, player);
            stage = EVENT.EXIT; // The next time 'Process' runs, the EXIT stage will run instead, which will then return the nextState.
        }
    }

    public override void Exit()
    {
        // anim.ResetTrigger("isWalking"); // Makes sure that any events queued up for Walking are cleared out.
        base.Exit();
    }
}

public class Pursue : State
{
    public Pursue(GameObject _enemy, NavMeshAgent _agent, Transform _player)
                : base(_enemy, _agent, _player)
    {
        name = STATE.PURSUE; // State set to match what enemy is doing.
        agent.speed = 5; // Speed set to make sure enemy appears to be running.
        agent.isStopped = false; // Set bool to determine enemy is moving.
    }

    public override void Enter()
    {
        // anim.SetTrigger("isRunning"); // Set running trigger to change animation.
        base.Enter();
    }

    public override void Update()
    {
        agent.SetDestination(player.position);  // Set goal for enemy to reach but navmesh processing might not have taken place, so...
        if(agent.hasPath)                       // ...check if agent has a path yet.
        {
            if (CanAttackPlayer())
            {
                nextState = new Attack(enemy, agent, player); // If enemy can attack player, set correct nextState.
                stage = EVENT.EXIT; // Set stage correctly as we are finished with Pursue state.
            }
            // If enemy can't see the player, switch back to Patrol state.
            else if (!CanSeePlayer())
            {
                nextState = new Patrol(enemy, agent, player); // If enemy can't see player, set correct nextState.
                stage = EVENT.EXIT; // Set stage correctly as we are finished with Pursue state.
            }
        }
    }

    public override void Exit()
    {
        // anim.ResetTrigger("isRunning"); // Makes sure that any events queued up for Running are cleared out.
        base.Exit();
    }
}

public class TakingDamage: State{
    public TakingDamage(GameObject _enemy, NavMeshAgent _agent, Transform _player)
                : base(_enemy, _agent, _player)
    {
        name = STATE.TAKINGDAMAGE;
    }
}

public class Attack : State
{
    float rotationSpeed = 2.0f; // Set speed that enemy will rotate around to face player.
    // AudioSource shoot; // To store the AudioSource component.
    public Attack(GameObject _enemy, NavMeshAgent _agent, Transform _player)
                : base(_enemy, _agent, _player)
    {
        name = STATE.ATTACK;
        // shoot = _enemy.GetComponent<AudioSource>(); // Get AudioSource component for shooting sound.
    }

    public override void Enter()
    {
        // anim.SetTrigger("isShooting"); // Set shooting trigger to change animation.
        agent.isStopped = true; // Stop enemy so he can shoot.
        // shoot.Play(); // Play shooting sound.
        base.Enter();
    }

    public override void Update()
    {
        // Calculate direction and angle to player.
        Vector3 direction = player.position - enemy.transform.position; // Provides the vector from the enemy to the player.
        float angle = Vector3.Angle(direction, enemy.transform.forward); // Provide angle of sight.
        direction.y = 0; // Prevent character from tilting.

        // Rotate enemy to always face the player that he's attacking.
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation,
                                            Quaternion.LookRotation(direction),
                                            Time.deltaTime * rotationSpeed);

        if(!CanAttackPlayer())
        {
            nextState = new Idle(enemy, agent, player); // If enemy can't attack player, set correct nextState.
            stage = EVENT.EXIT; // Set stage correctly as we are finished with Attack state.
        }
    }

    public override void Exit()
    {
        // anim.ResetTrigger("isShooting"); // Makes sure that any events queued up for Shooting are cleared out.
        // shoot.Stop(); // Stop shooting sound.
        base.Exit();
    }
}